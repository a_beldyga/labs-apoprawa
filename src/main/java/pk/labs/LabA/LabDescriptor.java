package pk.labs.LabA;



public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "display.Display";
    public static String controlPanelImplClassName = "controlpanel.Controlpanel";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.IMain";
    public static String mainComponentImplClassName = "glowny.glowny";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "app";
    // endregion
}
